import React from "react";
import PokemonData from "./data/PokemonData";

function App() {
  return <div>
    <PokemonData />
  </div>;
}

export default App;
