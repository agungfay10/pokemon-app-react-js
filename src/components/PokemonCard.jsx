import React from "react";

function PokemonCard({ id, name, image, type }) {
  return (
    <div className="w-60 p-2 bg-white rounded-xl transform transition-all hover:-translate-y-2 duration-300 shadow-lg hover:shadow-2xl">
      <h3 className="font-bold text-lg mb-2 text-black">#{id}</h3>
      <img className="h-40 object-cover rounded-xl" src={image} alt={name} />
      <div className="p-2 text-center">
        <h2 className="font-bold text-lg mb-2 text-black">{name}</h2>
        <p className="text-sm text-gray-600">{type}</p>
      </div>
    </div>
  );
}

export default PokemonCard;
