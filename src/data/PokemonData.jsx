import React, { useState, useEffect } from "react";
import PokemonCard from "../components/PokemonCard";
import { Audio } from "react-loader-spinner";

function PokemonData() {
  const [allPokemons, setAllPokemons] = useState([]);
  const [loading, setLoading] = useState(true);
  const [offset, setOffset] = useState(0);

  const getAllPokemons = async () => {
    try {
      const res = await fetch(
        `https://pokeapi.co/api/v2/pokemon?limit=20&offset=${offset}`
      );
      const data = await res.json();
      const pokemonPromises = data.results.map(async (pokemon) => {
        const res = await fetch(
          `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
        );
        return res.json();
      });

      const pokemonData = await Promise.all(pokemonPromises);

      setAllPokemons(pokemonData);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    } catch (error) {
      console.log("Error fetching Pokemon data: ", error);
      setLoading(false);
    }
  };

  const handleReload = () => {
    setOffset((prevOffset) => prevOffset + 10);
    setAllPokemons([]);
    setLoading(true);
  };

  useEffect(() => {
    getAllPokemons();
  }, [offset]);

  return (
    <div>
      <div className="text-center my-6">
        <h1 className="text-4xl font-semibold">Data Pokemon dari API Pokemon</h1>
        <div className="flex items-center justify-center">
          <button
            className="bg-blue-600 rounded-xl p-2 my-4"
            onClick={handleReload}
          >
            Muat Ulang
          </button>
        </div>
      </div>
      {loading ? (
        <div className="flex justify-center items-center h-screen">
          <Audio
            height="100"
            width="100"
            radius="9"
            color="blue"
            ariaLabel="loading"
            wrapperStyle
            wrapperClass
          />
        </div>
      ) : (
        <div className="w-full min-h-screen gap-4 flex-wrap flex justify-center items-center">
          {allPokemons.map((pokemon, index) => (
            <PokemonCard
              key={index}
              id={pokemon.id}
              image={pokemon.sprites.other.dream_world.front_default}
              name={pokemon.name}
              type={pokemon.types[0].type.name}
            />
          ))}
        </div>
      )}
    </div>
  );
}

export default PokemonData;
